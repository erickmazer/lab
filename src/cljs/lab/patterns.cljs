(ns lab.patterns)

(def font-sizes     { :x-small    "10px"
                      :small      "12px"
                      :medium     "15px"
                      :large      "19px"
                      :x-large    "24px"})


(def font-weights   { :x-light    "100"
                      :light      "300"
                      :medium     "500"
                      :heavy      "700"
                      :x-heavy    "900"})

(def colors         { :white                "#FFFFFF"
                      :x-light-gray         "#EEEEEE"
                      :light-gray           "#CCCCCC"
                      :gray                 "#AAAAAA"
                      :dark-gray            "#555555"
                      :x-dark-gray          "#222222"

                      :blue                 "blue"
                      :red                  "red"
                      :purple               "purple"})

(def spaces         { :x-small      "4px"
                      :small        "8px"
                      :medium       "16px"
                      :large        "24px"
                      :x-large      "32px"})
