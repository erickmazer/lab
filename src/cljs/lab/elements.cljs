(ns lab.elements
  (:require [lab.patterns :as patterns]
            [reagent.core :as r]
            [re-frame.core :refer [dispatch]]))

(defn button [text click-fn]
  [:input  {:type "button"
            :value text
            :on-click click-fn
            :style    {:background        "none"
                        :border           "1px solid"
                        :cursor           "pointer"
                        :border-radius    "4px"
                        :padding          "8px 24px"
                        :color            (:purple patterns/colors)
                        :font-weight      (:x-heavy patterns/font-weights)
                        :text-transform   "uppercase"
                        :font-size        (:x-small patterns/font-sizes)}}])

(defn input-text [value]
  [:input  {:type      "text"
            :value     @value
            :on-change (fn [event]
                         (reset! value (-> event .-target .-value)))}])


(defn widget-title [text icon]
  [:img icon]
  [:h1 {:style {:text-align       "left"
                :color            (:dark-gray patterns/colors)
                :font-size        (:small patterns/font-sizes)
                :font-weight      (:heavy patterns/font-weights)
                :text-transform   "uppercase"
                :margin           "0 0 16px 0"
                :padding          "0 0 16px 0"
                :border-bottom    (str "1px solid " (:x-light-gray patterns/colors))}}
    text])

(defn simple-field [label content]
  [:div {:style { :margin-bottom (:small patterns/spaces)
                  :color         (:dark-gray patterns/colors)}}
    [:p {:style { :text-align        "left"
                  :font-size         (:x-small patterns/font-sizes)
                  :text-transform    "uppercase"
                  :margin-bottom     (:x-small patterns/spaces)
                  :padding           "0"}}
      label]
    [:p {:style { :text-align       "left"
                  :margin           0
                  :padding          0}}
      content]])

(defn select-box [options]
  (into [:select]
   (for [item options]
     [:option {:value (:value item)} (:text item)])))

(defn pseudo-form []
  (let [value (r/atom "")]
    (fn pseudo-form-render []
      [:div
       [input-text value]
       [button "insert" (fn [event]
                          (dispatch [:add-option @value]))]])))
