(ns lab.widgets
  (:require [re-frame.core :as re-frame]
            [lab.styles :as styles]
            [lab.elements :as elements]))


(defn example-widget []
  (let [company   (re-frame/subscribe [:company])
        name      (re-frame/subscribe [:name])
        age       (re-frame/subscribe [:age])]
    (fn []
      [:section {:style (:basic-box styles/box)}
        [elements/widget-title "Profile"]
        [elements/simple-field "Name" @name]
        [elements/simple-field "Age" @age]
        [elements/simple-field "Company" @company]
        [elements/button "Uma opção"]
        [elements/button "Outra opção"]])))
