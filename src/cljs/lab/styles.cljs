(ns lab.styles
  (:require [lab.patterns :as patterns]))

(def main-panel {:wrapper       {:text-align "center"
                                 :padding-top "50px"
                                 :width "500px"
                                 :margin "0 auto"
                                 :font-family "'Open Sans', sans-serif"}
                 :divider       {:background "#CCC"
                                 :margin "40px auto"}})

(def box        {:basic-box     {:padding          "24px"
                                 :background       (:white patterns/colors)
                                 :border           (str "1px solid " (:light-gray patterns/colors))
                                 :border-radius    "4px"
                                 :color            (:darker-gray patterns/colors)}})
