(ns lab.handlers
    (:require [re-frame.core :as re-frame]
              [lab.db :as db]))

(re-frame/register-handler
 :initialize-db
 (fn  [_ _]
   db/default-db))

(re-frame/register-handler
 :add-option
 (fn [app-state [_ value]]
   (update app-state :options (fn [old-value]
                                (conj old-value {:value value :text value})))))
