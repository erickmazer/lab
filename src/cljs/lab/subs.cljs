(ns lab.subs
    (:require-macros [reagent.ratom :refer [reaction]])
    (:require [re-frame.core :as re-frame]))

(re-frame/register-sub
 :name
 (fn [db]
   (reaction (:name @db))))

(re-frame/register-sub
  :age
  (fn [db]
    (reaction (:age @db))))

(re-frame/register-sub
  :company
  (fn [db]
    (reaction (:company @db))))

(re-frame/register-sub
  :options
  (fn [db]
    (reaction (:options @db))))
