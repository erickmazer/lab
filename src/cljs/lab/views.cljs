(ns lab.views
  (:require [re-frame.core :as re-frame]
            [lab.styles :as styles]
            [lab.widgets :as widgets]
            [lab.elements :as elements]))

(defn main-panel []
  (let [name    (re-frame/subscribe [:name])
        options (re-frame/subscribe [:options])]
    (fn main-panel-render []
      [:div {:style (:wrapper styles/main-panel)}
        [:h1 "Hello from " @name]
        [:h2 "Não sei que merda to fazendo."]

        [widgets/example-widget]

        [:hr {:style (:divider styles/main-panel)}]

        [elements/select-box @options]
        [elements/pseudo-form]

        [:hr {:style (:divider styles/main-panel)}]

        [:h2 "Perguntas"]

        [:ol {:style {:text-align "left"}}
          [:li "Como consigo colocar @algumacoisa? Tentei colocar na db e não rolou"]
          [:li "Dá pra separar o CSS em algum arquivo? Tá ficando uma zona"]
          [:li "Pra que serve o REPL? Avaliar funções isoladas?"]
          [:li "A sintaxe tá fazendo sentido?"]]])))
